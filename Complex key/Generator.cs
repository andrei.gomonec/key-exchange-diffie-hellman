﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Complex_key
{

	public class Generator
	{	
		//Объявляем поля 
		private int _simpleModule;
		private int _generator;
		private bool _isInterception;
		
		private Random r = new Random();
		
		public void StartGenerate(bool isInterception)
		{
			_isInterception = isInterception;
			SeachSimpleModule();
		}
		
		private void SeachSimpleModule()
		{
			int _count = 0;
			_simpleModule = r.Next(100, 3000);

			for (int i = 1; i <= _simpleModule; i++) 
			{
				if(_simpleModule % i == 0)
					_count++;
				
				if(_count > 2)
				{
					SeachSimpleModule();					
					return;
				}	
			}
			
			if(_count == 2)
			{
				SendMessage("Простой модуль: " + _simpleModule);
				FindPrimitiveRoot(_simpleModule);
				return;
			}
		}
		
		private void FindPrimitiveRoot(int simpleModule)
		{
			_generator = r.Next(3, simpleModule);
			List<double>lt = new List<double>();
			if(IsCoprime(_generator, simpleModule))
			{
				for(int i = 1; i < simpleModule; i++)
				{
					BigInteger d = BigInteger.Pow(_generator, i) % simpleModule;
					lt.Add((double)d);
				}
				
				List<double> uniq = lt.Distinct().ToList();
					
				if(uniq.Count != simpleModule - 1)
				{
					FindPrimitiveRoot(_simpleModule);
					return;
				}
				SendMessage("Генератор: " + _generator);
				//SendMessage(Convert.ToString(lt.Count));
				//SendMessage(Convert.ToString(uniq.Count));
				
				KeyExchange key = new KeyExchange();
				key.DataInput(_generator, _simpleModule, _isInterception);
			}	
		}
		
		static bool IsCoprime(int a, int b)
		{
			if(a == b)
				return a == 1;
			else
			{
				if(a > b)
					return IsCoprime(a - b, b);
				else
					return IsCoprime(b-a, a);
			}
		}
		
		private void SendMessage(string text)
		{
			MainForm.window.ShowInfo(text);
		}
	}
}