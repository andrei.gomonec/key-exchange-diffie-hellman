﻿using System;
using System.Numerics;

namespace Complex_key
{
	public class KeyExchange
	{
		private int _generator;
		private int _simpleModule;
 		private int _numberA;
 		private int _numberB;
 		private int _numberZ;
 		private int _numberYA;
 		private int _numberYB;
 		private int _numberYZ;
 		private int _KKey;
 		private readonly Random _r = new Random();
 		
 		private bool _isAuto;
 		private bool _isInterception;
 		
		public void DataInput(int a, int q, int Xa, int Xb, int Xz, bool isInterception)
		{
			_isInterception = isInterception;
			_isAuto = false;
			_generator = a;
			_simpleModule = q;
			_numberA = Xa;
			_numberB = Xb;

			if(isInterception) _numberZ = Xz;
			
			GeneratePrivateKey();
		}
		
		public void DataInput(int generator, int simpleModule, bool isInterception)
		{
			_isInterception = isInterception;
			_isAuto = true;
			_generator = generator;
			_simpleModule = simpleModule;
			GeneratePrivateKey();
		}
		
		public void DataOutput(string text)
		{
			MainForm.window.ShowInfo(text);
		}
		
		private void GeneratePrivateKey()
		{
			if(_isAuto)
			{
				_numberA = _r.Next(3, _simpleModule);
				_numberB = _r.Next(3, _simpleModule);
				if(_isInterception)
				{
					_numberZ = _r.Next(3, _simpleModule);
					DataOutput("Секретные ключи: ");
					DataOutput( _numberA + ", " + _numberB + ", " + _numberZ);
				}
				else
				{
					DataOutput("Секретные ключи: ");
					DataOutput( _numberA + ", " + _numberB);
				}
			}

			BigInteger d = BigInteger.Pow(_generator, _numberA) % _simpleModule;
			_numberYA = (int)d;
			d = BigInteger.Pow(_generator, _numberB) % _simpleModule;
			_numberYB = (int)d;
				if(_isInterception)
				{
					d = BigInteger.Pow(_generator, _numberZ) % _simpleModule;
					_numberYZ = (int)d;
					DataOutput("Ключи для обмена: ");
					DataOutput(_numberYA + ", " + _numberYB + ", " + _numberYZ);
				}
				else
				{
					DataOutput("Ключи для обмена: ");
					DataOutput(_numberYA + ", " + _numberYB);
				}
				
				if(_isInterception == false)
				{
					d = BigInteger.Pow(_numberYB, _numberA) % _simpleModule;
					_KKey = (int)d;
					DataOutput("Пользователь 1 рассчитал общий ключ: " + _KKey);
				
					d = BigInteger.Pow(_numberYA, _numberB) % _simpleModule;
					_KKey = (int)d;
					DataOutput("Пользователь 2 рассчитал общий ключ: " + _KKey);
				}
				
				if(_isInterception == true)
				{
					d = BigInteger.Pow(_numberYZ, _numberA) % _simpleModule;
					_KKey = (int)d;
					DataOutput("Пользователь 1 рассчитал общий ключ: " + _KKey);
				
					d = BigInteger.Pow(_numberYA, _numberZ) % _simpleModule;
					_KKey = (int)d;
					DataOutput("У мошенника с 1-м пользователем: " + _KKey);
					
					d = BigInteger.Pow(_numberYB, _numberZ) % _simpleModule;
					_KKey = (int)d;
					DataOutput("У мошенника со 2-м пользователем: " + _KKey);
					
					d = BigInteger.Pow(_numberYZ, _numberB) % _simpleModule;
					_KKey = (int)d;
					DataOutput("Пользователь 2 рассчитал общий ключ: " + _KKey);
				}
		}
	}
}
