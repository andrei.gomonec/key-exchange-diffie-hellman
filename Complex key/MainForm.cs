﻿using System;
using System.Windows.Forms;

namespace Complex_key
{
	public partial class MainForm : Form
	{
		public Generator gen = new Generator();
		public KeyExchange key = new KeyExchange();
		public static MainForm window = null;
	
		private int _primeNumber;
		private int _simpleModule;
		private int _numberA;
		private int _numberB;
		private int _numberZ;
		private bool _isInterception;

		public MainForm()
		{
			InitializeComponent();
			window = this;
			panel1.Visible = false;
		}
		
		public void ShowInfo(string str)
		{
			info.Text += "\r\n" + str;
		}
		
		private void Button1Click(object sender, EventArgs e)
		{
			info.Text = null;
			if(checkBox.Checked)
			{
			this.Cursor = System.Windows.Forms.Cursors.AppStarting;
			string[] s = new string[1];
			gen.StartGenerate(_isInterception);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			}
			else
			{
				InputValidation();
			}
		}
		
		private void CheckBoxCheckedChanged(object sender, EventArgs e)
		{
			if(checkBox.Checked)
			{
				label5.Visible = false;
				numberZTextBox.Visible = false;
				panel1.Visible = false;
			} 
			else
			{
				panel1.Visible = true;
				if(checkBox2.Checked)
				{
					label5.Visible = true;
					numberZTextBox.Visible = true;
				}
			}		
		}
		
		private void InputValidation()
		{
			try 
			{
				_primeNumber = Convert.ToInt32(primeNumberTextBox.Text);
				_simpleModule = Convert.ToInt32(simpleModuleTextBox.Text);
				_numberA = Convert.ToInt32(numberATextBox.Text);
				_numberB = Convert.ToInt32(numberBTextBox.Text);
				
				if(checkBox2.Checked)_numberZ = Convert.ToInt32(numberZTextBox.Text);
				
				key.DataInput(_primeNumber, _simpleModule, _numberA, _numberB, _numberZ, _isInterception);
			} 
			catch (Exception) 
			{
				MessageBox.Show("Ошибка ввода пользовательских данных!","Ошибка");
			}	
		}
		
		private void CheckBox2CheckedChanged(object sender, EventArgs e)
		{
			if(checkBox2.Checked == true)
			{
				_isInterception = true;
				if(checkBox.Checked == false)
				{
					label5.Visible = true;
					numberZTextBox.Visible = true;
				}
			}

			else 
			{
				_isInterception = false;
				if(checkBox.Checked == false)
				{
					label5.Visible = false;
					numberZTextBox.Visible = false;
				}
			}
		}
	}
}