﻿/*
 * Создано в SharpDevelop.
 * Пользователь: 0764436
 * Дата: 23.11.2021
 * Время: 9:37
 * 
 * Для изменения этого шаблона используйте меню "Инструменты | Параметры | Кодирование | Стандартные заголовки".
 */
namespace Complex_key
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox info;
		private System.Windows.Forms.CheckBox checkBox;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.TextBox primeNumberTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox simpleModuleTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox numberBTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox numberATextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox numberZTextBox;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label6;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.info = new System.Windows.Forms.TextBox();
			this.checkBox = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.primeNumberTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.simpleModuleTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.numberBTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.numberATextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.numberZTextBox = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Cursor = System.Windows.Forms.Cursors.Default;
			this.button1.Location = new System.Drawing.Point(12, 92);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(342, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Генерация публичного ключа";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// info
			// 
			this.info.Location = new System.Drawing.Point(12, 121);
			this.info.Multiline = true;
			this.info.Name = "info";
			this.info.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.info.Size = new System.Drawing.Size(342, 236);
			this.info.TabIndex = 1;
			// 
			// checkBox
			// 
			this.checkBox.Checked = true;
			this.checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox.Location = new System.Drawing.Point(12, 7);
			this.checkBox.Name = "checkBox";
			this.checkBox.Size = new System.Drawing.Size(222, 24);
			this.checkBox.TabIndex = 5;
			this.checkBox.Text = "Генерировать данные автоматически";
			this.checkBox.UseVisualStyleBackColor = true;
			this.checkBox.CheckedChanged += new System.EventHandler(this.CheckBoxCheckedChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(12, 363);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(136, 24);
			this.checkBox2.TabIndex = 6;
			this.checkBox2.Text = "Перехват сообщений";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
			// 
			// primeNumberTextBox
			// 
			this.primeNumberTextBox.Location = new System.Drawing.Point(39, 8);
			this.primeNumberTextBox.Name = "primeNumberTextBox";
			this.primeNumberTextBox.Size = new System.Drawing.Size(33, 20);
			this.primeNumberTextBox.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(3, 5);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(30, 23);
			this.label1.TabIndex = 8;
			this.label1.Text = "a =";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(3, 27);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(30, 23);
			this.label2.TabIndex = 10;
			this.label2.Text = "q =";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// simpleModuleTextBox
			// 
			this.simpleModuleTextBox.Location = new System.Drawing.Point(39, 31);
			this.simpleModuleTextBox.Name = "simpleModuleTextBox";
			this.simpleModuleTextBox.Size = new System.Drawing.Size(33, 20);
			this.simpleModuleTextBox.TabIndex = 9;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(87, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(30, 23);
			this.label3.TabIndex = 14;
			this.label3.Text = "XB =";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numberBTextBox
			// 
			this.numberBTextBox.Location = new System.Drawing.Point(123, 30);
			this.numberBTextBox.Name = "numberBTextBox";
			this.numberBTextBox.Size = new System.Drawing.Size(33, 20);
			this.numberBTextBox.TabIndex = 13;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(87, 5);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(30, 23);
			this.label4.TabIndex = 12;
			this.label4.Text = "XA =";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numberATextBox
			// 
			this.numberATextBox.Location = new System.Drawing.Point(123, 7);
			this.numberATextBox.Name = "numberATextBox";
			this.numberATextBox.Size = new System.Drawing.Size(33, 20);
			this.numberATextBox.TabIndex = 11;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(181, 36);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(30, 23);
			this.label5.TabIndex = 16;
			this.label5.Text = "XZ=";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label5.Visible = false;
			// 
			// numberZTextBox
			// 
			this.numberZTextBox.Location = new System.Drawing.Point(217, 36);
			this.numberZTextBox.Name = "numberZTextBox";
			this.numberZTextBox.Size = new System.Drawing.Size(33, 20);
			this.numberZTextBox.TabIndex = 15;
			this.numberZTextBox.Visible = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.numberBTextBox);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.numberATextBox);
			this.panel1.Controls.Add(this.simpleModuleTextBox);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.primeNumberTextBox);
			this.panel1.Location = new System.Drawing.Point(12, 29);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(163, 57);
			this.panel1.TabIndex = 17;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(321, 368);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(33, 16);
			this.label6.TabIndex = 18;
			this.label6.Text = "AVG";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(366, 389);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.numberZTextBox);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.checkBox2);
			this.Controls.Add(this.checkBox);
			this.Controls.Add(this.info);
			this.Controls.Add(this.button1);
			this.Name = "MainForm";
			this.Text = "Complex key";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
