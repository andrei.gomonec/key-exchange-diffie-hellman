﻿using System;

namespace calc
{
	public class Program
	{
		public static void Main(string[] args)
		{
			
			Console.WriteLine("Первообразный модуль: ");
			//int a = Convert.ToInt32(Console.ReadKey());
			int a = Convert.ToInt32(Console.Read());

			Console.WriteLine("Генератор: ");
			int b = Convert.ToInt32(Console.Read());
			bool mass = true;
			ConsoleKeyInfo cki;
			while (mass = true) 
			{
				Console.WriteLine("В какую степень:");
				int c = Convert.ToInt32(Console.Read());
				Console.Write(Convert.ToString(Math.Pow(b, c) % a));
				
				ConsoleKeyInfo cki;
				cki = Console.ReadKey(true);
					if(cki.Key == ConsoleKey.Escape)
						mass = false;
			}

			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}